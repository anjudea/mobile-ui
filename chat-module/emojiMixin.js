import emojiImages from './emojiImages'
export default {
  data() {
    return {
      emojis: Object.keys(emojiImages)
    }
  },
  methods: {
    /**
     * 通过emoji图片路径获取emojiId
     * @param path
     * @returns {*}
     */
    getEmojiId(path) {
      if (!path) return
      for (let i in emojiImages) {
        if (path === emojiImages[i]) {
          return i
        }
      }
      return ''
    },
    /**
     * 通过emojiId获取emoji图片路径
     * @param id
     * @returns {*}
     */
    getEmojiPath(id) {
      if (!id) return
      return emojiImages[id]
    }
  }
}
