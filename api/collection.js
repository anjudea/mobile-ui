/**
 * 收藏相关API
 */
import request,{Method} from '@/utils/request'


/**
 * 获取店铺收藏
 * @param params
 * @returns {AxiosPromise}
 */
export function getShopCollection(params) {
  return request.ajax({
    url: '/buyer/members/collection/shops',
    method: Method.GET,
    needToken: true,
    params
  })
}


/**
 * 删除店铺收藏
 * @returns {AxiosPromise}
 */
export function deleteShopCollection(id) {
  return request.ajax({
    url: `/buyer/members/collection/shop/${id}`,
    method: Method.DELETE,
    needToken: true
  })
}


/**
 * 获取商品收藏
 * @param params
 * @returns {AxiosPromise}
 */
export function getGoodsCollection(params) {
  return request.ajax({
    url: '/buyer/members/collection/goods',
    method: Method.GET,
    needToken: true,
    params
  })
}

/**
 * 收藏商品
 * @param goods_id 商品ID
 * @returns {AxiosPromise}
 */
export function collectionGoods(goods_id) {
  return request.ajax({
    url: '/buyer/members/collection/goods',
    method: Method.POST,
    needToken: true,
    params: { goods_id }
  })
}

/**
 * 删除商品收藏
 * @param ids 收藏ID【集合或单个商品ID】
 * @returns {AxiosPromise}
 */
export function deleteGoodsCollection(ids) {
  if (Array.isArray(ids)) ids = ids.join(',')
  return request.ajax({
    url: `/buyer/members/collection/goods/${ids}`,
    method: Method.DELETE,
    needToken: true
  })
}

/**
 * 获取商品是否被收藏
 * @param good_id
 */
export function getGoodsIsCollect(good_id) {
  return request.ajax({
    url: `/buyer/members/collection/goods/${good_id}`,
    method: Method.GET,
    needToken: true
  })
}

/**
 * 获取店铺是否被收藏
 * @param shop_id
 */
export function getShopIsCollect(shop_id) {
  return request.ajax({
    url: `/buyer/members/collection/shop/${shop_id}`,
    method: Method.GET,
    needToken: true
  })
}
